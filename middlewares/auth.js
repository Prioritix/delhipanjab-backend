const jwt = require('jsonwebtoken');
const config = require('config');

const auth = (req,res,next)=>{
    try {
        const token = req.header('x-auth-token');
        const verifiedUser = jwt.verify(token,config.get('jwtsecret'))
        req.user = verifiedUser.user;
        next();
    } catch (error) {
        console.log("error in token verifier",error.message);
        res.status(401).json({errors:error});
    }
}

module.exports = auth;