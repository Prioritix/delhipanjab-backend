const express = require('express');
const bodyParser = require('body-parser');
const auth = require('../middlewares/auth');

var pool = require('../config/dbconfig');

const comments = express.Router();
comments.use(bodyParser.json());

//This module contains all the APIs for comment data handling

// API for Add a new comment
comments.post(
    '/add',
    auth,
    (req,res)=>{
        try {
            let post_id = req.body.post_id;
            let author = req.user.username;
            let comment = req.body.comment;

            pool.getConnection((err,connection)=>{
                if (err) console.log(err); 

                query = 'INSERT INTO ' + 'comments' + ' (post_id, author, comment) VALUES (? , ? , ?)';
                connection.query(query, [post_id,author,comment], function (error, results){
                    // connection.release();
                    if (!error){
                        if (results){
                            query = 'UPDATE ' + 'posts' + ' set comment_count = comment_count + 1 where id = ?';
                            connection.query(query, [post_id], function (error, results2){
                                connection.release();
                                if (!error){
                                    if (results){
                                        return res.status(200).json({msg:"Comment is successfuly recorded"});
                                    }
                                }
                                else{
                                    console.log(error)
                                   return res.status(401).json({errors:error}); 
                                }
                            })      
                            
                        }
                    }
                    else{
                        console.log(error)
                        return res.status(401).json({errors:error});
                    }
                })                    
            })

            
        } catch (error) {
            console.log(error)
            return res.status(401).json({errors:error}); 
        }
    }
)

//API for reading comments for a specific post
comments.post(
    '/read',
    (req,res)=>{
        try {
            let post_id = req.body.post_id;

            pool.getConnection((err,connection)=>{
                if (err) console.log(err); 

                    query = 'SELECT * FROM comments where post_id = ?';
                    connection.query(query,[post_id],function (error, results){
                        connection.release();
                        if (!error){
                            if (results){
                                return res.status(200).json(results);
                            }
                    }
                    // console.log(error)
                    return res.status(401).json({errors:error});
                })                    
            })

            
        } catch (error) {
            console.log(error)
            return res.status(401).json({errors:error}); 
        }
    }
)
module.exports = comments;