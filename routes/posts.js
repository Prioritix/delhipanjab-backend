const express = require('express');
const bodyParser = require('body-parser');
const auth = require('../middlewares/auth');
const {check,validationResult} = require('express-validator');

var pool = require('../config/dbconfig');

const posts = express.Router();
posts.use(bodyParser.json());

// This module contains all the APIs for posts data handling

//API for adding a new post
posts.post(
    '/add',
    auth,
    [ 
        check('title',"Title cannot be empty").not().isEmpty(),
        check("description","Description cannot be empty").not().isEmpty(),
        check("imgURL","Image URL cannot be empty").not().isEmpty()
    ],
    (req,res)=>{
        try {
            let {title,description,imgURL} = req.body;
            let username = req.user.username;
            const errors = validationResult(req);
            if (errors.isEmpty()){
                pool.getConnection((err,connection)=>{
                    if (err) console.log(err); 

                    let date = new Date();

                    query = 'INSERT INTO ' + 'posts' + ' (title, description, imgURL, created, author, likes,views) VALUES (? , ? , ? ,?, ?, ?,?)';
                    connection.query(query, [title, description, imgURL, date, username, 0, 0], function (error, results){
                        connection.release();
                        if (!error){
                            if (results){
                                return res.status(200).json({msg:"Post is successfuly added"});
                            }
                        }
                        // console.log(error)
                        return res.status(500).json({errors:error});
                    })                    
                })
            }
            else{
                console.log(errors["errors"][0])
                res.status(501).json({errors:errors["errors"][0]});
            }

        } catch (error) {
            console.log(error)
            res.status(401).json({errors:error.message})
        }
    }
);


//API for adding a like to a post
posts.post(
    '/like',
    // auth,
    (req,res)=>{
        try {
            let post_id = req.body.post_id
            pool.getConnection((err,connection)=>{
                if (err) console.log(err); 

                    query = 'UPDATE ' + 'posts' + ' set likes = likes + 1 where id = ?';
                    connection.query(query, [post_id], function (error, results){
                        connection.release();
                        if (!error){
                            if (results){
                                return res.status(200).json({msg:"Successfuly liked"});
                            }
                        }

                        return res.status(401).json({errors:error});
                    })                    
            })
        } catch (error) {
            console.log(error)
            res.status(401).json({errors:error.message})
        }
    }
);


//API for reading all posts
posts.get(
    '/readAll',
    (req,res)=>{
        try {
            pool.getConnection((err,connection)=>{
                if (err) console.log(err); 

                    query = 'SELECT * FROM posts ORDER BY id DESC;';
                    connection.query(query, function (error, results){
                        connection.release();
                        if (!error){
                            if (results){
                                return res.status(200).json(results);
                            }
                    }
                    // console.log(error)
                    return res.status(500).json({errors:error});
                })                    
            })

        } catch (error) {
            console.log(error)
            res.status(401).json({errors:error.message})
        }
    }
);


//API for reading one specific post
posts.post(
    '/readOne',
    (req,res)=>{
        try {
            let post_id =req.body.post_id
            pool.getConnection((err,connection)=>{
                if (err) console.log(err); 

                    query = 'SELECT * FROM posts where id = ?';
                    connection.query(query,[post_id], function (error, results){
                        // connection.release();
                        if (!error){
                            if (results){
                                query = 'UPDATE ' + 'posts' + ' set views = views + 1 where id = ?';
                                connection.query(query, [post_id], function (error, results_2){
                                    connection.release();
                                    if (!error){
                                        if (results_2){
                                            return res.status(200).json(results);
                                        }
                                    }
                                    console.log("error at 131 >>",error)
                                    return res.status(401).json({errors:error});
                                    
                                })
                            }
                        }
                        else{
                            console.log("This error at 137",error)
                        return res.status(500).json({errors:error});
                        }
                        
                    })                    
            })

        } catch (error) {
            console.log(error)
            res.status(401).json({errors:error.message})
        }
    }
);


module.exports = posts;