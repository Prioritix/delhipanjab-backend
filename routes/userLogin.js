const express = require('express');
const bodyParser = require('body-parser');
const {check,validationResult} = require('express-validator');
const jwt = require('jsonwebtoken');
const config = require('config');
const bcrypt = require('bcrypt');
const auth = require('../middlewares/auth')
const saltRounds = 10;

var pool = require('../config/dbconfig');

const userLogin = express.Router();
userLogin.use(bodyParser.json());

//This module contains all the APIs for user authentication

//API for User login
userLogin.post(
    '/login',
    [ 
        check('email',"Enter a valid email address").isEmail(),
        check("password","Password cannot be empty").not().isEmpty()
    ],
    (req,res)=>{
        try {
            const {email,password} = req.body;
            const errors = validationResult(req);
            if (errors.isEmpty()){
                pool.getConnection((err,connection)=>{
                    if (err) console.log(err); 

                    query = 'SELECT * FROM ' + "users" + ' WHERE email = ? ';
			        connection.query(query , [email], function (error, results){
                        if (!error){
                            if (results.length>0){
                                connection.release();
                                let username = results[0].username;
                                let db_pass = results[0].password;
                                bcrypt.compare(password, db_pass, function(err, correctPassword) {
                                    if (correctPassword){
                                        const payload={
                                            user:{
                                                id:results[0].id,
                                                username:username,
                                                email:email
                                            }
                                        };
                                        jwt.sign(payload,config.get('jwtsecret'),(err,token)=>{
                                            return res.json({token:token});
                                        });
                                    }
                                    else{
                                        if (err) console.log(err);
                                        res.status(401).json({errors:{param:"login-fail",msg:'Incorrect password'}});
                                    }
                                });
                            }
                            else{
                                connection.release();
                                res.status(401).json({errors:{param:"login-fail",msg:`The email ${email} is not registered!`}})
                            }
                        }
                        else{
                            connection.release();
                            response.status(401).json({errors:error});	
                            response.end();
                        }
                    })
                })
            }
            else{
                console.log(errors["errors"][0])
                res.status(401).json({errors:errors["errors"][0]});
            }
        } catch (error) {
            console.log("error >>",error)
            res.status(401).json({errors:error.message})
        }
    }
);

//API for user registration
userLogin.post(
    '/register',
    [ 
        check('email',"Enter a valid email address").isEmail(),
        check("password","Password cannot be empty").not().isEmpty(),
        check("username","Username cannot be empty").not().isEmpty(),
    ],
    (req,res)=>{
        try {
            const {username,email,password} = req.body;
            const errors = validationResult(req);
            if (errors.isEmpty()){
                pool.getConnection((err,connection)=>{
                    if (err) console.log(err); 

                    query = 'SELECT * FROM ' + "users" + ' WHERE email = ? ';
			        connection.query(query , [email], function (error, results){
                        if (!error){
                            if (results.length>0){
                                connection.release();
                                return res.status(401).json({errors:{param:"register-failed",msg:`The email ${results[0].email} is already registered!`}})
                            }
                            else{
                                bcrypt.hash(password, saltRounds, function(err, hash) {
                                    if (err){
                                        connection.release();
                                        console.log("Error at password hashing",err)
                                        return res.status(401).json({errors:{param:"register-fail",msg:'Failed at password hashing'}})
                                    }
                                    else{
                                        query = 'INSERT INTO ' + 'users' + ' (email, username, password) VALUES (? , ? , ?)';
										connection.query(query, [email, username, hash], function (error, results){
                                            connection.release();
                                            if (!error){
                                                if (results){
                                                    const payload={
                                                        user:{
                                                            id:results.insertId,
                                                            username:username,
                                                            email:email
                                                        }
                                                    };
                                                    jwt.sign(payload,config.get('jwtsecret'),(err,token)=>{
                                                        if (err) console.log(err)
                                                        return res.json({token:token});
                                                    });
                                                }
                                            }
                                        })
                                    }
                                })
                            }
                        }
                        else{
                            connection.release();
                            response.status(401).json({errors:error});	
                            response.end();
                        }
                    })
                })
            }
            else{
                console.log(errors["errors"][0])
                res.status(401).json({errors:errors["errors"][0]});
            }
        } catch (error) {
            console.log("error >>",error)
            res.status(401).json({errors:error.message})
        }
    }
);

//API for user loading
userLogin.get(
    "/load",
    auth,
    (req,res)=>{
        try {
            let user = req.user;
            pool.getConnection((err,connection)=>{
                if (err) console.log(err); 
                query = 'SELECT id,username,email FROM ' + "users" + ' WHERE id = ? ';
                connection.query(query , [user.id], function (error, results){
                    connection.release();
                    if (!error){
                        if(results.length>0){
                            return res.status(200).json(results)
                        }
                    }
                    else{
                        res.status(401).json({errors:error})
                    }
                })
            })
        } catch (error) {
            res.status(401).json({errors:error})
        }
    }
)

module.exports = userLogin;