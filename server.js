const express = require('express');
const bodyParser = require('body-parser');
const morgan = require('morgan');
const cors = require('cors');

// Import route modules
const userLogin = require('./routes/userLogin');
const posts = require('./routes/posts');
const comments = require('./routes/comments');

// create a app instance
const app = express()
let PORT = process.env.PORT|| 5000;

//Import middlewares
app.use(cors());
app.use(morgan('dev'));
app.use(bodyParser.json());

//Dirrect the API end points
app.use('/api/user',userLogin);
app.use('/api/posts',posts);
app.use('/api/comments',comments);

//Server end point
app.all('/',
    (req,res)=>{
        try {
            res.status(200).send("Connected to Server successfuly!") 
        } catch (error) {
            res.status(401).send("Error in server connection!")
        }
    }
);

// Start the server
app.listen(PORT,()=>{
    console.log("Server is running on port :",PORT)
});