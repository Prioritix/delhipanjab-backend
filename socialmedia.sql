-- phpMyAdmin SQL Dump
-- version 5.0.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Dec 15, 2020 at 10:01 AM
-- Server version: 10.4.16-MariaDB
-- PHP Version: 7.4.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `socialmedia`
--

-- --------------------------------------------------------

--
-- Table structure for table `comments`
--

CREATE TABLE `comments` (
  `id` int(50) NOT NULL,
  `post_id` int(50) NOT NULL,
  `author` varchar(50) NOT NULL,
  `comment` varchar(254) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `comments`
--

INSERT INTO `comments` (`id`, `post_id`, `author`, `comment`) VALUES
(1, 1, 'Theekshana', 'Best movie'),
(2, 1, 'Test', 'Tin tin is awesome. My favorite movie'),
(3, 21, 'Test', 'Hello sherlock i am moriaty'),
(4, 19, 'Test', 'I watched this movie two times');

-- --------------------------------------------------------

--
-- Table structure for table `posts`
--

CREATE TABLE `posts` (
  `id` int(100) NOT NULL,
  `title` varchar(50) NOT NULL,
  `description` varchar(254) NOT NULL,
  `imgURL` varchar(254) NOT NULL,
  `author` varchar(50) NOT NULL,
  `created` datetime(6) NOT NULL,
  `likes` int(100) NOT NULL,
  `views` int(50) NOT NULL,
  `comment_count` int(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `posts`
--

INSERT INTO `posts` (`id`, `title`, `description`, `imgURL`, `author`, `created`, `likes`, `views`, `comment_count`) VALUES
(1, 'Test title', 'This is a description sample', 'https://metvcdn.metv.com/KbQ83-1551727016-2412-blog-scooby%20doo%20voice.jpg', 'unknown person', '2020-12-14 07:56:04.624000', 5, 9, 2),
(6, 'Test title 2', 'This is a description sample 2', 'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRrGJipvKF-R0bJhjLtddGskRjNzXEuEeIVrg&usqp=CAU', 'unknown person', '2020-12-14 07:56:04.624000', 22, 0, 0),
(7, 'Test title 3', 'This is a description sample 2', 'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRrGJipvKF-R0bJhjLtddGskRjNzXEuEeIVrg&usqp=CAU', 'unknown person', '2020-12-14 07:56:04.624000', 10, 0, 0),
(8, 'Test title 3', 'This is a description sample 2', 'https://upload.wikimedia.org/wikipedia/en/e/ed/Iron_Man_2_poster.jpg', 'unknown person', '2020-12-14 07:56:04.624000', 17, 0, 0),
(9, 'Test title', 'This is a description sample', 'https://metvcdn.metv.com/KbQ83-1551727016-2412-blog-scooby%20doo%20voice.jpg', 'unknown person', '2020-12-14 07:56:04.624000', 4, 1, 0),
(10, 'Avatar Movie', 'Avatar is a 2009 American epic science fiction film ', 'https://upload.wikimedia.org/wikipedia/en/thumb/b/b0/Avatar-Teaser-Poster.jpg/220px-Avatar-Teaser-Poster.jpg', 'unknown person', '2020-12-14 20:55:13.164000', 1, 2, 0),
(11, '6 Underground', '6 Underground is a 2019 American action thriller film', 'https://occ-0-531-2219.1.nflxso.net/dnm/api/v6/X194eJsgWBDE2aQbaNdmCXGUP-Y/AAAABWACkSA4Ow_uiIcj-Q6NL74UHsICjGOl4VC-a2yDZdbbNuLm3FART8p_nsk129behhn4GE2K0eoaltnrsbaL4j0uZU8IR1pBZnScPmJJYKteHKadTsKddPSYsmctTw.jpg?r=d9a', 'unknown person', '2020-12-14 20:58:57.373000', 1, 12, 0),
(12, 'Avengers', 'The Avengers take a final stand against Thanos in Marvel Studios\' conclusion to 22 films', 'https://images.indianexpress.com/2018/04/avengers-iron-man-cap-america.jpg?w=350', 'Test', '2020-12-15 00:15:24.945000', 2, 8, 0),
(13, 'Doctor Strange', 'Doctor Strange is a 2016 American superhero film based on the Marvel Comics character of the same name.', 'https://imagesvc.meredithcorp.io/v3/mm/image?url=https%3A%2F%2Fstatic.onecms.io%2Fwp-content%2Fuploads%2Fsites%2F6%2F2018%2F04%2Fhka0760_comp_v003_030105-10-2000.jpg&q=85', 'Test', '2020-12-15 12:09:48.335000', 0, 0, 0),
(14, 'Fast & Furious', 'It is distributed by Universal Pictures. The first film was released in 2001, which began the original trilogy of films focused on illegal street racing', 'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQtvolpT0Mxmy6o9u878ux5LQ77Jdu73DYcQw&usqp=CAU', 'Test', '2020-12-15 12:15:20.619000', 0, 1, 0),
(18, ' film in the Matrix', 'The Matrix is a 1999 American science fiction action film written and directed by the', 'data:image/jpeg;base64,/9j/4AAQSkZJRgABAQAAAQABAAD/2wCEAAkGBxMTEhUTExMWFhUXGBgXGBgYGBcYGBgbGBUXFxgaGBoYHiggGBolHRUXITEhJSkrLi4uGB8zODMtNygtLisBCgoKDg0OGhAQGy0lHyUtLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLf/AABEIAJ8BPgMBIgACEQEDEQH', 'Test', '2020-12-15 12:26:16.468000', 0, 2, 0),
(19, 'The Lion King', 'The Lion King is a 1994 American animated musical drama film produced by Walt Disney', 'https://lumiere-a.akamaihd.net/v1/images/b_thelionking2019_header_poststreet_18276_ada305ce.jpeg', 'Test', '2020-12-15 12:29:43.455000', 0, 3, 1),
(20, ' The Adventures of Tintin', 'The Adventures of Tintin is a series of 24 bande dessinée albums created by Belgian cartoonist Georges Remi, who wrote under the pen name Hergé', 'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQzqGg4WMhb6nTJ2LgOpwgWFRoX9FtkQdMO8g&usqp=CAU', 'Test', '2020-12-15 12:39:05.385000', 0, 8, 0),
(21, 'Sherlock Holmes', 'Sherlock Holmes is a fictional private detective created by British author Sir Arthur Conan Doyle. Referring to himself as a \"consulting detective\" in the stories', 'https://imgix.bustle.com/inverse/8d/2e/cc/37/6ca2/41c7/81cc/8c9a28f2dfbc/cumberbatch-holmesjpg.jpeg?w=1200&h=630&q=70&fit=crop&crop=faces&fm=jpg', 'Test', '2020-12-15 12:41:25.268000', 3, 12, 1);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(255) NOT NULL,
  `username` varchar(100) NOT NULL,
  `email` varchar(100) NOT NULL,
  `password` varchar(150) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `username`, `email`, `password`) VALUES
(3, 'Test', 'test@gmail.com', '$2b$10$HQbVoTOrYasZAgYMUZ9OyuHbE3PZTlrrlS6e5xnZthKl0AUrMGGzy'),
(23, 'Test', 'test1@gmail.com', '$2b$10$hAM1/u4wAls4AkcZMeZSXOeqap19lw5Lzg3AVCOj78lQTqfh5Ab6G'),
(24, 'ghhh', 'test8@gmail.com', '$2b$10$VCdbBBrfPPFM8Gt/4GJAWOQPDjOwWXGtRpIN7q.L0efDX9TdHyBeO');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `comments`
--
ALTER TABLE `comments`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `posts`
--
ALTER TABLE `posts`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `comments`
--
ALTER TABLE `comments`
  MODIFY `id` int(50) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `posts`
--
ALTER TABLE `posts`
  MODIFY `id` int(100) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=25;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
